"""This program provides detecting and parsing title blocks in (scanned) construction plans

It losely follows this guide for preprocessing and detecting:
https://blog.qburst.com/2019/10/ocr-extracting-printed-text-from-scanned-documents/
4 Steps:
    1. Image quality enhancement – Improve the quality of the images using image processing libraries
      1.1. Grayscale
      1.2. Binarization
      1.3. Image orientation – Detect and correct image orientation
      (1.4. Noise Removal)
    2. Image segmentation – Separate different form fields with contour boxes
    3. Data extraction – Extract data in required format
"""

import cv2
import numpy as np

import pytesseract
from pytesseract import Output

import logging
import time

import os
import pkg_resources
import argparse

import stringHandling as sh
import exportCSV as eCSV


def prep(tmp_path: str = 'tmp'):
    """Doing some preparations:

    - checking if temporary folder exists (holds outputs and logfiles)
    - enable logging
    - checking tesseract and pytesseract versions

    Args:
        tmp_path: relative path to temporary folder

    Returns:
        None
    """
    debug_level = ''
    logfile_path = tmp_path + '/logfile.log'
    wd_path = os.getcwd()

    # if temporary folder doesn't exist try to create it
    if not os.path.isdir(tmp_path):
        try:
            os.makedirs(tmp_path)
        except:
            print('Could not write on path {0}! Check your writing privileges.'.format(wd_path))
            exit(1)

        enable_logging(logfile_path, log_level=debug_level)

        logging.warning('folder {} does not exist'.format(tmp_path))
        logging.info('successfully created folder \'{}\''.format(tmp_path))
    # if temporary folder already exists just enable logging
    else:
        enable_logging(logfile_path, log_level=debug_level)
        logging.info('folder {} does exist'.format(tmp_path))

    logging.info('current working directory: {}'.format(wd_path))

    print('\nLogfile is saved at {}\n'.format(os.path.abspath(logfile_path)))

    # check if pytesseract and tesseract are installed properly
    check_tesseract_vers()


def enable_logging(file_path, log_level='INFO'):
    """Enables different levels of logging to a logfile.

    Args:
        file_path:  (relative) path to logfile including filename
        log_level:  desired level of logging (INFO or detailed (DEBUG))

    Returns:
        None
    """
    try:
        if log_level == 'INFO':
            # less detailed logging
            logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                                datefmt='%Y-%m-%d %I:%M',
                                level=logging.INFO, filename=file_path, filemode='w')
            logging.info('Logging to file \'{}\' enabled'.format(
                file_path))
        else:
            # detailed logging
            logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                                level=logging.DEBUG,
                                filename=file_path, filemode='w')
            logging.info('Logging to file \'{}\' with level of logging set to DEBUG enabled'.format(
                file_path))
    except Exception as e:
        print(e)


def check_tesseract_vers():
    """
    Checks for working installations of pytesseract and tesseract; logs and prints found versions

    Terminates program when pytesseract or tesseract are not working
    """
    try:
        pyt_vers = pkg_resources.working_set.by_key['pytesseract'].version
        found_str = 'Found a pytesseract installation, version {}'.format(pyt_vers)
        print(found_str)
        logging.info(found_str)
    except:
        error_str = 'Error: pytesseract installation not found.'
        print(error_str)
        logging.warning(error_str)
        exit(1)

    try:
        t_vers = pytesseract.get_tesseract_version()
        found_str = 'Found a TESSERACT installation, version {}'.format(t_vers)
        print(found_str)
        logging.info(found_str)
    except:
        error_str = 'Error: TESSERACT installation not found.' \
                    'If you think you installed it correctly, check your PATH variable.'
        print(error_str)
        logging.warning(error_str)
        exit(1)


def import_img(img_path: str):
    """Imports image through OpenCV returns image as numpy.ndarray, height, width and no. of channels.

    Args:
        img_path: path to image file

    Returns:
        image, height, width, no. of channels
    """
    print('\n--- Trying to import image \'{}\' ...'.format(img_path))
    if cv2.haveImageReader(img_path):
        img = cv2.imread(img_path)  # import image as ndarray

        # get dimensions of image (height, width, number of channels)
        h, w, ch = img.shape

        succ_str = 'Image \'{}\' successfully imported. Size: {} x {} pixel (height x width).'.format(img_path, h, w)
        print(succ_str)
        logging.info(succ_str)

        return img, h, w, ch

    else:
        error_str = 'Error: File at path \'{}\' not found or not an image!'.format(img_path)
        print(error_str)
        logging.error(error_str)

        exit(1)


def prepr_pipe(img: np.ndarray):
    """Pipeline for Preprocessing the current image

    Args:
        img: image to which preprocessing should be applied

    Returns:
        :rtype: (np.ndarray, np.ndarray)

        image, intense eroded image
    """
    def grayscale(img_colored):
        t_1 = time.time()
        img_gray = cv2.cvtColor(img_colored, cv2.COLOR_BGR2GRAY)
        t_2 = time.time()
        print('Processing time for gray-scaling: {:.4f} seconds.'.format(t_2 - t_1))

        return img_gray

    def binarization(img_nonbin):
        t_1 = time.time()
        # 'thresh=' is used, if cv2.THRESH_OTSU or cv2.THRESH_TRIANGLE are not used.
        # Otherwise a threshold is evaluated and returned
        threshold, img_bin = cv2.threshold(img_nonbin, thresh=127, maxval=255, type=cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        t_2 = time.time()
        print('Processing time for binarization: {:.4f} seconds. Evaluated Threshold: {:3d}. '
              .format(t_2 - t_1, int(threshold)))

        return img_bin

    def dilation(img_undilated):
        t_1 = time.time()

        img_dilated = cv2.dilate(img_undilated, cv2.getStructuringElement(cv2.MORPH_CROSS, (2, 2)))

        t_2 = time.time()
        print('Processing time for dilation: {:.4f} seconds.'.format(t_2 - t_1))

        return img_dilated

    def erosion(img_orig, shape: (int, int) =(3,3), iter=1):
        t_1 = time.time()

        kernel_square = np.ones((2, 2), np.uint8)
        kernel_cross = cv2.getStructuringElement(cv2.MORPH_CROSS, shape)
        img_eroded = cv2.erode(img_orig, kernel_cross, iterations=iter)

        t_2 = time.time()
        print('Processing time for erosion: {:.4f} seconds.'.format(t_2 - t_1))

        return img_eroded

    def denoise(img_noised):
        t_1 = time.time()

        img_denoised = cv2.fastNlMeansDenoising(img_noised, h=10, templateWindowSize=7, searchWindowSize=21)

        t_2 = time.time()
        print('Processing time for denoising: {:.4f} seconds.'.format(t_2 - t_1))

        return img_denoised

    print('\n--- Start preprocessing image ...')
    img = grayscale(img)

    img = binarization(img)

    img = erosion(img, iter=1)

    img_intense_erosion = erosion(img, shape=(1,5), iter=1)
    img_intense_erosion = erosion(img_intense_erosion, shape=(5, 1), iter=1)
    img_intense_erosion = erosion(img_intense_erosion, shape=(1, 5), iter=1)
    img_intense_erosion = erosion(img_intense_erosion, shape=(5, 1), iter=1)
    # img = dilation(img)
    # img = denoise(img)

    return img, img_intense_erosion


def save_image(img, path):
    """Saves array or np.ndarray as image file.
    *Warning:* cv2.imwrite() only writes in existing directories! (more info: https://stackoverflow.com/a/17516231)

    Args:
        img: image to save
        path: absolute or relative saving path

    Returns:
        None
    """
    folder_path =  os.path.dirname(path)
    abs_path = os.path.abspath(folder_path)

    if not os.path.isdir(folder_path):
        warning_str = 'could  not write image \'{}\': folder or path \'{}\' does not exist'.format(img, abs_path)
        logging.warning(warning_str)
        print(warning_str)
    else:
        logging.info('successfully saved image \'{}\' in \'{}\''.format(os.path.basename(path), abs_path))
        cv2.imwrite('{}'.format(path), img)


def show_image(img, name: str):
    """Shows image in openCV window. Just for debugging purposes.

    Args:
        img: image to show
        name: arbitrary name of current window

    Returns:
        None
    """
    cv2.imshow('image: {}'.format(name), img)
    cv2.waitKey(0)


def find_contours(img):
    """
    Looks for all contours in given image. See https://stackoverflow.com/a/62809396 for more information.

    Args:
        img: image in which you want to find contours

    Returns:
        all found contours as a list of np.ndarrays with all edge points
    """
    print('\nSearch for contours...')
    t_1 = time.time()

    # cv2.RETR_CCOMP: retrieves all the contours and arranges them to a 2-level hierarchy
    # cv2.RETR_LIST: ignores hierarchy completely
    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_contours/py_contours_hierarchy/py_contours_hierarchy.html
    cnts_raw = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # different openCV versions give different output (tuple with 2 or 3 elements)
    if len(cnts_raw) == 2:
        cnts, hierarchy = cnts_raw
        print('{} contour(s) overall found'.format(len(cnts)))
    elif len(cnts_raw) == 3:
        _, cnts, hierarchy = cnts_raw
        print('{} contour(s) overall found'.format(len(cnts)))
    else:
        cnts = None
        print('No contours found overall!')

    t_2 = time.time()
    print('Processing time for contouring: {:.4f} seconds.'.format(t_2 - t_1))

    return cnts


def crop_contours(img, img_with_cnts, cnts, area_boundary: (int, int) =(1, int(10 ** 20)), min_length: int =5):
    """Evaluates given contours if they are nearly rectangular and within a given size and have a minimal side length.

    - calculate contour area with Green's theorem
    - evaluate minimal rectangular bounding box around found contour
    - evaluate if contour area and bounding box area are nearly the same (< 95% difference to be nearly a rectangle) and big enough
    - if contour is nearly a rectangle add it to contour_list

    Args:
        img:    image to crop
        img_with_cnts:  image on which found contours will be drawn. should be rgb to see coloured contours
        cnts:   list of contours found by openCV
        area_boundary:  area in Pixels*pixels of bounding box
        min_length: minimum length of each boundary side

    Returns:
        Tuple with list of cropped images, their contours and an np.ndarray with the image with contours drawn on it
    """
    print('\nCrop image on rectangular contours...')
    t_1 = time.time()

    contour_list = []
    cropped_images_list = []
    idx = 1

    if cnts is None:
        print('No cropping executed because no contours were found previously!')
        return None, None, None

    for contour in cnts:
        # calculates contour area with Green's theorem
        # https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html#ga2c759ed9f497d4a618048a2f56dc97f1
        area = cv2.contourArea(contour)

        # minimum length of bounding box sides highly recommended,
        # because form fields have to be wide enough for readable texts
        # dimensions of bounding box: x (positive from left to right), y (positive from top to bottom), width, height
        # bounding_box = rectangle(contour, area, min_length=min_length)
        bounding_box = rotated_rectangle(contour, area, min_length=min_length)

        # draw rotated rectangular bounding boxes (blue)
        cv2.drawContours(img_with_cnts, [np.int0(cv2.boxPoints(cv2.minAreaRect(contour)))], 0, (204, 0, 0), 1)

        if bounding_box:
            x, y, w, h = bounding_box
            if is_in_area(area, *area_boundary):
                # draw rectangular bounding boxes (lime green)
                cv2.drawContours(img_with_cnts, [contour], 0, color=(36, 255, 12), thickness=1)

                # check if part image contains only useless whitespace (0 = black, 255 = white)
                cropped_img = img[y:y + h, x:x + w]
                if cropped_img.min() < 255:
                    cropped_images_list.append(cropped_img)
                    contour_list.append(contour)

        else:
            # draw just the contour (orange)
            cv2.drawContours(img_with_cnts, [contour], 0, color=(36, 100, 255), thickness=1)
            pass

        idx += 1
        print('\rChecking {} of {} possible croppings ...'.format(idx, len(cnts)), end="", flush=True)

    print('\n\n{} rectangular contour(s) found, with  following constraints:'
          '\n>> area between {} and {} px * px'
          '\n>> minimum side length of {} px (width and height)'
          .format(len(contour_list), *area_boundary, min_length))

    t_2 = time.time()
    print('Processing time for cropping: {:.4f} seconds.'
          .format(t_2 - t_1))

    return cropped_images_list, contour_list, img_with_cnts


def rectangle(contour, area, min_length: int =5):
    """Find (nearly) rectangular contours by comparing contour area with *not rotated* bounding box area
    (imprecise method if box is skewed!)

    Args:
        contour:
        area: contour area previously calculated
        min_length: minimal side length of bounding box

    Returns:

    """
    x, y, w, h = cv2.boundingRect(contour)

    offset = 1  # offset because cv2.contourArea gives inner area whereas cv2.boundingRect gives outerArea
    if w > min_length and h > min_length:
        ratio = area / ((w - offset) * (h - offset))
    else:
        ratio = 0.0

    if ratio > 0.95:
        return x, y, w, h
    else:
        return False


def rotated_rectangle(contour, area, min_length: int =5):
    """Find (nearly) rectangular contours by comparing contour area with *rotated* bounding box area

    Args:
        contour:
        area: contour area previously calculated
        min_length: minimal side length of bounding box

    Returns: x-coord, y-coord, width, height

    """
    rot_rect, (w, h), rot_angle = cv2.minAreaRect(contour)

    offset = 1  # offset because cv2.contourArea gives inner area whereas cv2.boundingRect gives outerArea
    if w > min_length and h > min_length:
        ratio = area / ((w - offset) * (h - offset))
    else:
        ratio = 0.0

    if ratio > 0.95:
        x, y, w, h = cv2.boundingRect(contour)
        return x, y, w, h
    else:
        return False


def is_in_area(area, area_min, area_max):
    if area_min < area < area_max:
        return True
    else:
        return False


def skew_detection(img, contours, tmp_folder):
    # TODO add sekw detection here
    # img_canny = cv2.Canny(img, threshold1=1, threshold2=2)
    # lines = cv2.HoughLinesP(img_canny, rho=5, theta=np.pi/180.0, threshold=80, minLineLength=30, maxLineGap=10)
    # for i in lines:
    #     cv2.line(img_orig, (i[0][0], i[0][1]), (i[0][2], i[0][3]), color=(0, 0, 255), thickness=2)
    # save_image(img_orig, temp_folder+'lines.jpg')

    for cnt in contours:
        epsilon = 0.005 * cv2.arcLength(cnt, True)
        approx_corners = cv2.approxPolyDP(cnt, epsilon, True)
        edges, _, _ = np.shape(approx_corners)
        if edges == 4:
            cv2.drawContours(img, [approx_corners], -1, (255, 255, 0), 2)
        pass
    save_image(img, tmp_folder+'Polygons.jpg')


def ocr_global(img, language: str, oem:int =3, psm: int =3, user_words_path: str = None, own_config: str = None):
    print('\n--- Using TESSERACT for OCR on whole image...')
    start_time = time.time()

    if own_config:
        conf_str = 'Tesseract logging path: look in config file \'{1}\' in folder \'{0}\' for variable \'debug_file\''\
            .format(os.getcwd(), own_config)
        print(conf_str)
        logging.info(conf_str)
    else:
        conf_str = 'Tesseract logging not enabled.'
        print(conf_str)
        logging.info(conf_str)

    # get all possible text from image (could be very vague/inaccurate)
    custom_oem_psm_config = r'--oem {} --psm {} --user-words {} {}'.format(oem, psm, user_words_path, own_config)
    d_global = pytesseract.image_to_data(
        img, lang=language, output_type=Output.DICT, config=custom_oem_psm_config)
    string_global = ' '.join(d_global['text'])

    end_time = time.time()
    print('Duration for global search with Tesseract: {:.4f} seconds'
          .format(end_time - start_time))

    return string_global, d_global


def ocr_local(img_list, language: str, oem: int =3, psm: int =3, user_words_path: str = None, own_config: str = None):
    print('\n--- Using TESSERACT for OCR on cropped images ...')
    start_time = time.time()

    custom_oem_psm_config = r'--oem {} --psm {} --user-words {} {}'.format(oem, psm, user_words_path, own_config)

    string_local_list = []
    d_local_list = []
    idx = 0
    for img_cropped in img_list:
        print('\rApplied OCR on {} of {} image parts ...'.format(idx+1, len(img_list)), end="", flush=True)

        d_local_list.append(pytesseract.image_to_data(
            img_cropped, lang=language, output_type=Output.DICT, config=custom_oem_psm_config))
        string_local_list.append(d_local_list[idx]['text'])
        idx += 1

    end_time = time.time()
    print('Duration for search with Tesseract in previous detected form fields: {:.4f} seconds'
          .format(end_time - start_time))

    return string_local_list, d_local_list


def word_boxes(images, d_list):
    n_boxes = len(images)
    for i in range(n_boxes):
        for k in range(len(d_list[i]['level'])):
            # draw rectangles around every recognized word for every cropped partial image:
            (x, y, w, h) = (d_list[i]['left'][k], d_list[i]['top'][k], d_list[i]['width'][k], d_list[i]['height'][k])
            cv2.rectangle(images[i], (x, y), (x + w, y + h), (0, 100, 0), 1)
        # show bounding boxes for every recognized word for every cropped partial image 'i':
        cv2.imshow('img: {}'.format(i), images[i])
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def main():
    start_time_cpu = time.process_time()
    start_time = time.time()

    # parse arguments given via command line or terminal input
    parser = argparse.ArgumentParser()
    parser.add_argument("imgpath",
                        help="Path to image which you want to parse. For relative paths use something like this: "
                             "folder_name/subfolder/image_name.jpg")
    args = parser.parse_args()


    # enable logging, prepare working directory and check correct installation
    temp_folder = 'tmp/'
    prep()

    # load image
    img_path = args.imgpath     # 'testimagesOCR/nurnbergerei_2.jpg'
    img_orig, h, w, ch = import_img(img_path=img_path)
    img = img_orig.copy()

    # preprocess image
    img, img_intense_erosion = prepr_pipe(img)
    save_image(img, path= temp_folder+'img_prepr.png')
    save_image(img_intense_erosion, path= temp_folder+'img_intense_erosion.png')

    # find contours
    # boundaries set to one third of image size
    area_bnds = (1000, int((h*w)/3))
    min_side_length = 10
    contours = find_contours(img_intense_erosion)

    # TODO add skew detection
    # skew_detection(...)

    cropped_images, contour_list, img_with_cnts = crop_contours(img, img_orig, contours, area_bnds, min_side_length)

    # use ocr global
    string_global, d_global = ocr_global(img, language='deu', oem=3, psm=3, user_words_path='user_words',
                                         own_config='tess_config')
    # word_boxes([img], [d_global])

    # use ocr local
    if cropped_images:
        string_local, d_local = ocr_local(cropped_images, language='deu', oem=3, psm=3, user_words_path='user_words')
        # word_boxes(cropped_images, d_local)

        save_image(img_with_cnts, path= temp_folder+'output.png')

    # string handling
    regex_dict = sh.regex_dictionary()

    # string handling global
    if string_global:
        dict_list_global = []
        dict_list_global.append(sh.regex_search(regex_dict, string_global))

        # write strings to .csv
        eCSV.write_to_csv(dict_list_global, temp_folder+'out_global.csv')

    # string handling local
    if cropped_images:
        dict_list_local = []
        for line in string_local:
            string = ' '.join(line)
            dict_list_local.append(sh.regex_search(regex_dict, string))

        # write strings to .csv
        eCSV.write_to_csv(dict_list_local, temp_folder+'out_local.csv')

    end_time_cpu = time.process_time()
    end_time = time.time()
    print('\nTotal time elapsed: {:.4f} seconds.\n'
          'Total CPU time: {:.4f} seconds. (note: all cpu cores are summed up when multithreading is performed)'
          .format(end_time - start_time, end_time_cpu - start_time_cpu))

    print("\n====================================\n"
          "Processed image files and csv files are saved to folder '{}'.".format(temp_folder))



if __name__ == '__main__':
    main()
