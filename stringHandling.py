"""
Module that searches for keywords often found in form fields (information in construction plans) in regular strings
"""

import re  # regex support


def regex_dictionary():
    """
    {keyname (string):
        [regex keyname pattern (raw string), regex key value pattern (raw string), eliminate whitespace (boolean)]}
    Returns: dictionary with regex patterns
    """
    regex_dict = \
        {'Massstab': # catches variants and typos in words Skala, scale, Maßstab, mst.
                     [r'(?i)m[ao](?:s*|ß|ßs)t[ao]b|s[ck][ao]l[e[ao]]|m(?:st|s)\.?|\bm',
                      # catches whitespaces in scales, e.g. 1 :100 or 200 : 2 and ignores numbers with leading zeros
                      r'(?<!0)[1-9]\d*\s*:\s*[1-9]\d*', True],

         'Datum': [r'(?i)d[ao]t(?:um|e|\.?)',
                   # finds date with form dd mm yy or yyyy
                   r'(?:0?[1-9]|[12][0-9]|3[01])[-\/.](?:0[1-9]|1[012])[-\/.](?:\d\d\d\d|\d\d)', True],

         'Architekt': [r'(?i)[ao]rch[il]tekt', None, False],

         'Bauherr': [r'(?i)b[ao]u[hn]e(?:rr|m)', None, False],

         'Bauteil': [r'(?i)b[ao]ute[il]{2}', None, False],

         'Blattnr.': [r'(?i)b[il][ao]tt\W?(?:n(?:ummer|um)?\.?)?|bl?\W{0,2}nr?\W', None, False]

         }

    return regex_dict


def regex_search(regex_dict, string: str):
    """
    search for certain patterns in given string
    needed pattern for dictionary:
    {keyname (string):
        [regex keyname pattern (raw string), regex key value pattern (raw string), eliminate whitespace (boolean)]}
    """
    dict = {}

    for keyword_line in regex_dict:
        f_keyvalue_list = []
        keyword, keyvalue, elem_whitespace = regex_dict[keyword_line]

        # search for keyword
        f_keyword = re.findall(keyword, string)

        # if keyvalue for keyword is specified (i.e. not None), search for keyvalues
        if keyvalue:
            f_keyvalue = re.findall(keyvalue, string)

            # if keyvalue(s) exist, eliminate whitespace if needed and add to a list
            for line in f_keyvalue:
                if elem_whitespace:
                    f_keyvalue_line = re.sub(r"\s+", "", line,
                                             flags=re.UNICODE)  # eliminate all whitespace from scale string
                else:
                    f_keyvalue_line = line
                f_keyvalue_list.append(f_keyvalue_line)
        else:
            if f_keyword:
                f_keyvalue_list.append(string)
            f_keyvalue = None

        # print(keyword_line, f_keyword, f_keyvalue)

        dict[keyword_line] = ' '.join(f_keyvalue_list)

    return dict


def regex_test():
    """
    test function for quick regex testing

    Returns: None
    """
    string = """Zeichenerklärung    LBP Maßnahme 3.1E Ausrüstung    Anpflanzung von Einzelbäumen und  Großstäuchern im Baufeld    Betonpflasterplatten  mit Natursteinvorsatz    Bahnsteigplatie  BP 1300 mit Ril  g 2000x itenplatte    LSA-Mast    Kd-18: Bez.-Nr. Fahrleitungsmast/  we Ing  ÖB18: Bez.-Nr. Beleuchtungsmast  LSA8-5: 8 Knotenpunktnummer  5 Mastnummer LSA    Rückbau Mast    Fahrradbügel                                 IN RR III = N Sicherung von Einzeiäumen durch temporären Einschlag und  N N Mean:  Fällıng von Einzeibäumen / Erseiz am ursprünglichen Standort  Standortverbesserung für Einzebaum  nn    Hinweis zur Lage von LBP-Maßnahmen vol. auch UL2/ UL3/ UL 9.1    "a {    Kartengrundlage  Entwurfsvermessung: Ingenieur-Vermessung Dresden Henke-Hofmann GmbH  14    gemessen: 01-02/2014;ergänzt: 04/2014, 07/20  jebezug Ri nbezug NHN  indkarte: Städtisches Vermessungamt Dresden              Koma | -  OrIED Orden INGENIEURBÜRO |boatotot [16.122014| weine |  en Be |    Internet: www.ibk-dresden.net    Art der Änderung | Datum | Zeichen _    FESTSTELLUNGSENTWURF    Straßenbauverwaltung Unterlage / Blatt-Nr.: 5.1 / 1    Landeshauptstadt Dresden   Geschäftsbereich Stadtentwicklung Lageplan  Straßen- und Tiefbauamt   PF 12 00 20, 01001 Dresden   Tel.: 0351 /4880 Maßstab: 1:250         Stadtbahn 2020 TA 1.1 Zentralhaltestelle Kesselsdorfer Straße  von Reisewitzer Straße bis einschließlich KP Tharandter Straße    aufgestellt:  Dresden, ...    Prof. Reinhard Koettnitz  Amtsleiter Straßen- u. Tiefbauamt         15095_13_1405_1_1.PLTWS_1_1 12.12.14    """
    regex_search(regex_dictionary(), string)


def main():
    regex_test()


if __name__ == '__main__':
    main()
