"""
Module that exports a list of dictionarys to .csv file

Input format: list of dictionaries, e.g.:
`[{name:'bob', age: '35' height: '187'}, {name:'alice', age: '44', height: '170'}`

Output format as .csv:
```
name, age, height
bob, 35, 187
alice, 44, 170
```
"""
import csv


def write_to_csv(dict, output_file_name):

    with open(output_file_name, 'w', encoding='utf8', newline='') as output_file:
        fc = csv.DictWriter(output_file, fieldnames=dict[0].keys(), dialect='excel')
        fc.writeheader()
        fc.writerows(dict)


# toCSV = [{'name':'bob','age':25,'weight':200}, {'name':'jim','age':31,'weight':180}]
# write_to_csv(toCSV, 'out')