# Title Block detection with OCR

## Purpose
I made this little Python Script for detecting and parsing title blocks in (scanned) construction plans for my project thesis. 

## Requirements
Working installation of Python >= 3.6 and [TESSERACT](https://github.com/tesseract-ocr/tesseract) >= 4.x with configured PATH variable requiered. You may type `tesseract -v` in your terminal or command line after installation to check if TESSERACT was installed correctly. Under windows use Version >= 5.x (alpha) if possible.

At least 8 GB of RAM is recommended.

## Usage
To invoke the title block detection script, just type
````
$ python mainOCRScript.py <path/to/file.jpg>
````
Example:
````
$ python mainOCRScript.py testimagesOCR/nurnbergerei_2.jpg
````

## Misc
All docstrings in this project follow [Google's styleguide for python](https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings).
This makes using [Doxygen](https://www.doxygen.nl/index.html) together with [doxypypy](https://pypi.org/project/doxypypy/) with Python code easier.